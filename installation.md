0. Install InfluxDB - 
```
curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
source /etc/lsb-release
echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo apt-get update && sudo apt-get install influxdb
```
1. Start InfluxDB - sudo service influxd start
2. Check InfluxDB status - sudo service influxd status
3. Check if InfluxDB is amongst running processes - ps -ef|grep influx
4. Ping InfluxDB - curl -i 'http://localhost:8086/ping'
5. Config file - /etc/influxdb/influxdb.conf
6. Default user: root, password: root