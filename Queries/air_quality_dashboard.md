Temporary Series
```
SELECT mean("$gas") AS "$parametro" FROM "autogen"."air" WHERE $timeFilter GROUP BY time($__interval), "city" fill(null)
```

Histogram
```
SELECT mean("$gas") AS "$gas" FROM "autogen"."air" WHERE $timeFilter GROUP BY time($__interval), "station" fill(null)
```

Gas (eg. PM10)
```
SELECT mean("pm10") AS "PM10" FROM "autogen"."air" WHERE $timeFilter GROUP BY time($__interval), "city" fill(null)
```